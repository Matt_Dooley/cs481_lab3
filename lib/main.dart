import 'package:flutter/material.dart';

Color beige = const Color.fromRGBO(236, 204, 148, 1.0);
Color yellow = const Color.fromRGBO(252, 204, 44, 1.0);
Color orange = const Color.fromRGBO(252, 116, 20, 1.0);

void main() {
  runApp(MyApp());
}

/// This is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CS481_Lab3',
      home: Scaffold(
        /*
        appBar: AppBar(
          title: Text('CS481_Lab3'),
        ),

         */
        body: Center(
          child: //Text('Hello World'),
          MyStatefulWidget(),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
  TextStyle(fontSize: 30, fontWeight: FontWeight.bold);


  List<Widget> _widgetOptions = <Widget>[
    MyHomePage(),
    //_InputChipExample(),
    MyChipWidget(),
    //MyCardWidget(),
    MyDataTable(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /*
      appBar: AppBar(
        title: const Text('BottomNavigationBar Sample'),
      ),

       */
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: Colors.black,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('Home'),
            icon: Icon(Icons.home),
          ),
          BottomNavigationBarItem(
            title: Text('Types'),
            icon: Icon(Icons.category),
          ),
          BottomNavigationBarItem(
            title: Text('States'),
            icon: Icon(Icons.map),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: yellow,
        unselectedItemColor: orange,
        onTap: _onItemTapped,
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: beige,
      body: ListView (
        children: [
          Image.asset(
            'images/kids.jpg',
            fit: BoxFit.cover,
          ),

          //invisible spacing box
          SizedBox(height: 20),

          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'Halloween Candy',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 36,
                      fontWeight: FontWeight.bold,
                      color: orange,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'by the Numbers',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),

          //invisible spacing box
          SizedBox(height: 20),

          Container(
            padding: const EdgeInsets.only(left: 18.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child: Text(
                    'Most Popular Halloween Candy State-by-State',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      color: orange,
                    ),
                  ),
                ),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child: Text(
                          ("It’s a weird year for Halloween and trick-or-treating. Different areas are in different states of covid-19 cases either expanding or contracting. You should monitor your local situation and follow health guidelines if you plan to trick-or-treat. And wear an effective mask, not just a costume mask.\n\n"
                              "According to the National Retail Federation, trick-or-treaters are expected to be down 20% this year. That said, only 11% fewer people plan to hand out candy.  I’m no math expert, but 11 is less than 20. So, there might be more candy per house to capture!  Right? Halloween candy sales are expected to reach 2.4 Billion, only down slightly from 2.6B last year. No matter how you look at it, that’s a lot of candy!\n\n"
                              "The data graph reveals the results of candystore.com's annual data mining, the top 3 most popular Halloween candies in each state."
                              "")
                        ),
                      ),

                    ],
                  ),
                ),
              ],
            ),
          ),

          //invisible spacing box
          SizedBox(height: 20),

          RaisedButton(
            child: new Text(
              "Halloween Candy Quick Facts",
            ),
            textColor: Colors.black,
            color: orange,
            onPressed: () {
              showAlertDialog1(context);
            },
          ),

          //invisible spacing box
          SizedBox(height: 60),

        ], // Children
      ),
    );

  }
  showAlertDialog1(BuildContext context) {
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text(
        'Halloween Candy Quick Facts',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.bold,
          color: orange,
        ),
      ),
      content:
      Container(
        child:
        Text(
          ("172 million Americans celebrate Halloween\n\n"
              "30% of all Halloween purchases are made online\n\n"
              "Among those who celebrate Halloween, 95% will purchase candy\n\n"
              "They will spend about 27.55 on average\n\n"
              "Most Halloween shopping is done the first 2 weeks of October.\n\n"
              "In Oregon, full-sized candy bars are the norm for trick-or-treaters to receive\n\n"
              "Overall Halloween spending is expected to be 8.0B in 2020\n\n"
              "Over 50% of parents stash some Halloween candy to enjoy later in the year"),

          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      actions: [
        okButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}

class MyChipWidget extends StatefulWidget {
  @override
  MyChipWidgetState createState() =>
      new MyChipWidgetState();
}

class MyChipWidgetState extends State<MyChipWidget> with TickerProviderStateMixin {

  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    HardCandyCard(),
    ChocolateCard(),
    ChewyCandyCard(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
color: beige,
    child: Column(


        children: <Widget>[
          //invisible spacing box
          SizedBox(height: 25),
        Wrap(
        spacing: 8.0, // gap between adjacent chips
        runSpacing: 4.0, // gap between lines
        children: <Widget>[
          ChoiceChip(
            selected: _selectedIndex == 0,
            label: Text("Hard Candy, Suckers & Lollipops", style: TextStyle(color: Colors.white)),
            //avatar: FlutterLogo(),
            elevation: 10,
            pressElevation: 5,
            backgroundColor: orange,
            selectedColor: yellow,
            onSelected: (bool selected) {
              setState(() {
                if (selected) {
                  _selectedIndex = 0;
                }
                _onItemTapped;
              });
            },
          ),
          ChoiceChip(
            selected: _selectedIndex == 1,
            label: Text("Candy & Chocolate Bars", style: TextStyle(color: Colors.white)),
            //avatar: FlutterLogo(),
            elevation: 10,
            pressElevation: 5,
            backgroundColor: orange,
            selectedColor: yellow,
            onSelected: (bool selected) {
              setState(() {
                if (selected) {
                  _selectedIndex = 1;
                }
              });
            },
          ),
          ChoiceChip(
            selected: _selectedIndex == 2,
            label: Text("Jelly Beans, Chewy and Gummy Candy", style: TextStyle(color: Colors.white)),
            //avatar: FlutterLogo(),
            elevation: 10,
            pressElevation: 5,
            backgroundColor: orange,
            selectedColor: yellow,
            onSelected: (bool selected) {
              setState(() {
                if (selected) {
                  _selectedIndex = 2;
                }
              });
            },
          ),
        ],
      ),
    Flexible(
      child:
    Scaffold(
    body:

    SingleChildScrollView (
              child: _widgetOptions.elementAt(_selectedIndex)

          ),
    ),
    ),
    ],

      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class ChocolateCard extends StatelessWidget {
  ChocolateCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(
      color: beige,
      child: Column(
        children: [
          MusketeersCard(),
          AlmondJoyCard(),
          ButterfingerCard(),
          HersheyKissesCard(),
          HersheysMiniBarsCard(),
          KitKatCard(),
          MandMCard(),
          MilkyWayCard(),
          ReesesCupsCard(),
          SnickersCard(),
          TwixCard(),
        ],

      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class HardCandyCard extends StatelessWidget {
  HardCandyCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(

      color: beige,
      child: Column(
        children: [
          BlowPopsCard(),
          JollyRanchersCard(),
          TootsiePopsCard(),
        ],

      ),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class ChewyCandyCard extends StatelessWidget {
  ChewyCandyCard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Container(

      color: beige,
      child: Column(
        children: [
          CandyCornCard	(),
          DubbleBubbleGumCard(),
          HotTamalesCard(),
          LemonheadsCard(),
          LifeSaversCard(),
          SaltWaterTaffyCard(),
          SkittlesCard(),
          SourPatchKidsCard(),
          StarburstCard(),
          SwedishFishCard(),
        ],

      ),
    );
  }
}

class SnickersCard extends StatelessWidget {
  SnickersCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/snickers.jpg',
              fit: BoxFit.fill,
            ),
            ExpansionTile(
              title: Text("Snickers", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Packed with roasted peanuts, nougat, caramel and "
                          "milk chocolate. SNICKERS® Brand handles your hunger, "
                          "so you handle the things in life that aren't related "
                          "to hunger at all.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),

                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class MusketeersCard extends StatelessWidget {
  MusketeersCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/3musketeers.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("3 Musketeers", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Introduced in 1932, the now-famed 3 Musketeers bar "
                          "got its name because it originally came with three "
                          "pieces: one chocolate, one strawberry, and one "
                          "vanilla, all in the same package. When war-time sugar"
                          " rations pinched production, the manufacturer decided"
                          " to only make chocolate, the version still clamored "
                          "for today.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class BlowPopsCard extends StatelessWidget {
  BlowPopsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/blowpops.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Blow Pops", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Charms Blow Pops have a chewy, bubble gum center "
                          "surrounded by a delicious, fruit-flavored, hard candy"
                          " shell. Available in a variety of great-tasting "
                          "flavors that include traditional fruit varieties and "
                          "more contemporary favorites--Kiwi Berry Blast, Black "
                          "Ice, What-a-Melon, and the brand’s most popular, Blue"
                          " Razz--the pops are the world’s most popular "
                          "gum-filled lollipop.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class AlmondJoyCard extends StatelessWidget {
  AlmondJoyCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/almondjoy.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Almond Joy", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Nothing says Halloween joy like ALMOND JOY Candy "
                          "Bars! Theyʼre sweet coconut and whole almonds wrapped"
                          " in a chocolate candy coating, so itʼs pretty "
                          "exciting to find one in your treat bucket. Give "
                          "trick-or-treaters a taste of adventure with ALMOND "
                          "JOY Snack Size Bars. Get a bag or two today to "
                          "complete your Halloween prep.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
                //Text('Birth of the Sun'),
                //Text('Earth is Born'),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class ButterfingerCard extends StatelessWidget {
  ButterfingerCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/butterfinger.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Butterfinger", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("There’s nothing like the crispety, crunchety, "
                          "peanut-buttery taste of Butterfinger. Still makes you"
                          " say 'Nobody's Gonna Lay A Finger On My Butterfinger'.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class CandyCornCard extends StatelessWidget {
  CandyCornCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/candycorn.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Candy Corn", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("You’ve heard the apple doesn’t fall too far from "
                          "the tree. Well in this case it’s the candy corn that "
                          "hasn’t fallen far from the Maize. Half the size of "
                          "the staple Halloween treat, these little sweets manage"
                          " to pack in the same amount of flavor with their "
                          "colorfully layered triangular bodies. Made with real "
                          "honey, these mini candy corns are fresh and delicious,"
                          " sure to be your go-to favorite this Halloween season. ",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class JollyRanchersCard extends StatelessWidget {
  JollyRanchersCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/jollyranchers.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Jolly Ranchers", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("What's a jolly rancher? A happy guy with cows? No, "
                          "it's a legendary hard candy, bursting with sweet-tart"
                          " flavor that lasts for the life of the sucker – right"
                          " down to that itty-bitty, rice-sized piece you'll be "
                          "nursing with your tongue. So start suckin'!",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class HotTamalesCard extends StatelessWidget {
  HotTamalesCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/hottamales.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Hot Tamales", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Spicy cinnamon flair in a yummy chewy candy!",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class LifeSaversCard extends StatelessWidget {
  LifeSaversCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/lifesavers.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Life Savers", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("With the classic shape of a life preserver ring, "
                          "LifeSavers Gummies offer chewy bite-size gummy rings "
                          "in an assortment of fabulous fruit flavors.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class LemonheadsCard extends StatelessWidget {
  LemonheadsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/lemonheads.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Lemonheads", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Lemonheads are are an old fashioned candy classic! "
                          "Featuring a bright yellow color, these tangy, "
                          "sour-on-the-outside, sweet-on-the-inside candies "
                          "achieve a taste of lemony perfection.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class SkittlesCard extends StatelessWidget {
  SkittlesCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/skittles.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Skittles", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Skittles have recently been voted the most popular "
                          "candy amongst youth in the United States. But did you"
                          " know that these fruity morsels have also been voted "
                          "the best chewy candy, as well as the trendiest "
                          "decorating material, by the mythical creature "
                          "community? (Though we’re told leprechauns and "
                          "unicorns may have rigged the mythical polls, as they "
                          "are rather partial to rainbow-colored sweets.) ",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class SourPatchKidsCard extends StatelessWidget {
  SourPatchKidsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/sourpatchkids.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Sour Patch Kids", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("First sweet, then sour are the adolescent Sour Patch"
                          " people. Snatched from their homeland, sprinkled in "
                          "sugar, and stuffed into a resealable baggy for your "
                          "snacking convenience, these defenseless little "
                          "gummies satisfy both sour and sweet cravings "
                          "immediately. There’s no shame in munching on a whole "
                          "school of gummified children, for they are just a "
                          "small part of the great circle of candy. And they "
                          "taste far better than those dreadful cabbage patch "
                          "children.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class StarburstCard extends StatelessWidget {
  StarburstCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/starburst.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Starburst", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Starbursts candy... a galaxy full of fruity flavor "
                          "is packed into these stars of the confectionery "
                          "universe. A chewy burst of refreshing fruit flavor, "
                          "Starburst Fruit Chews are classic, bite-size, "
                          "square-shaped taffies that have been making mouths "
                          "water since 1959. They are easily recognized as the "
                          "quintessential summertime candy snack, delivering "
                          "sweet, refreshing fruit flavors in every piece.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class ReesesCupsCard extends StatelessWidget {
  ReesesCupsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/reesescups.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Reese's Cups", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Every peanut butter and chocolate lovers favorite "
                          "candy cup. A delectable combination of rich chocolate"
                          " and thick peanut butter makes my mouth water.... How"
                          " about yours? ",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class TwixCard extends StatelessWidget {
  TwixCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/twix.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Twix", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Without its twin, the lone Twix is still quite fun. "
                          "Smaller in stature, but full of taste, this "
                          "individually gold wrapped goodie will be the "
                          "quintessential snacking mate. Although it doesn’t "
                          "have its normal pairing, that shouldn’t stop you from"
                          " getting your fair share. Because who can resist a "
                          "delectable cookie wrapped in caramel and milk "
                          "chocolate bliss? ",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class TootsiePopsCard extends StatelessWidget {
  TootsiePopsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/tootsiepops.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Tootsie Pops", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("There are certain things in life that you just never"
                          " grow out of; the thrill of riding a bicycle "
                          "downhill; the bliss of watching the clouds roll by; "
                          "and the simple joy of unwrapping your very own "
                          "Tootsie Pop. That soft paper wrapper always reveals"
                          " a colorful orange, chocolate, raspberry, green "
                          "apple, grape, or cherry-flavored lollipop that hides "
                          "a soft, chewy chocolate Tootsie interior. And, much "
                          "like growing up, getting there is half the fun.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class SwedishFishCard extends StatelessWidget {
  SwedishFishCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/swedishfish.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Swedish Fish", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("I am Inga from Sveden, und I say to you -- Eat more"
                          " fish! Svedish fish! Yeah, we're not sure why the "
                          "Swedes make chewy candy fish: We just know we like "
                          "'em. Soft, fruity, flexible – you can arrange them "
                          "in artistic patterns, use them as puppets in a little"
                          " show, or just toss 'em down the hatch.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class MilkyWayCard extends StatelessWidget {
  MilkyWayCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/milkyway.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Milky Way", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("'Houston, we have a candy bar'. Need an out of this"
                          " world candy?! No candy is more far-out than "
                          "mouth-watering Milky Way Bars!",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class MandMCard extends StatelessWidget {
  MandMCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/mandm.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("M&M's", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Yummy mini M&M's milk chocolate candies that will "
                          " you say 'mmm-mmmm'. Great for toppings and baking, "
                          "these colorful M&M's minis add a fun flair to any "
                          "treat!",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class KitKatCard extends StatelessWidget {
  KitKatCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/kitkat.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Kit Kat", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Give me a break, give me a break, break me off a "
                          "piece of that Kit Kat bar!\n\nThis classic jingle has"
                          " been cited as one of the top 10 ear-worms of all "
                          "time -– one of those songs that get stuck in your "
                          "head for hours or days at a time.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class DubbleBubbleGumCard extends StatelessWidget {
  DubbleBubbleGumCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/dubblebubblegum.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Dubble Bubble Gum", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("An old time bubblegum favorite, Dubble Bubble gum "
                          "has been generating bubble-blowing fun for "
                          "generations. Relive your sweet childhood memories "
                          "with every chewy bite. Simply unwrap, chew, and make"
                          " the bubbles burst!",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class SaltWaterTaffyCard extends StatelessWidget {
  SaltWaterTaffyCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/saltwatertaffy.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Salt Water Taffy", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("If you’re a fan of all things sweet and chewy, then "
                          "you probably enjoy taffy. Perhaps you have a favorite"
                          " flavor, or perhaps you enjoy dabbling in a variety "
                          "of tastes.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class HersheyKissesCard extends StatelessWidget {
  HersheyKissesCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/hersheykisses.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Hershey Kisses", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Classic Hershey's Kisses milk chocolate morsels! "
                          "Bright silver foil en-wraps a yummy milk chocolate "
                          "morsel and a little white paper ribbon with the word"
                          " 'KISSES' printed on it.",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),
          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

class HersheysMiniBarsCard extends StatelessWidget {
  HersheysMiniBarsCard({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'images/hersheysminibars.jpg',
              //width: 600,
              //height: 200,
              fit: BoxFit.fill,
              //fit: BoxFit.cover,
            ),
            ExpansionTile(
              title: Text("Hershey's Mini Bars", style: TextStyle(fontWeight: FontWeight.bold, color: yellow),),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(20.0),
                  child: (
                      Text("Like your chocolate milk or dark? Smooth or with a "
                          "bit of crunch? Maybe a little nutty? Now, how about "
                          "your friends, do they have the same taste as you?",
                          style: TextStyle(fontWeight: FontWeight.bold, color: orange))
                  ),
                ),
              ],
            ),

          ]),
      elevation: 5,
      margin: EdgeInsets.all(10),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyDataTable extends StatelessWidget {
  MyDataTable({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(

      children: <Widget>[
      Flexible(
        child: Scaffold(
          backgroundColor: beige,
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(
              'Most Popular Halloween Candy State-by-State',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: orange,
              ),
            ),
          ),

          body: Padding(
          padding: const EdgeInsets.all(15.0),

          child: SingleChildScrollView (
            child: Container(
              decoration: const BoxDecoration(
                border: Border(
                  top: BorderSide(width: 1.0, color: Colors.black),
                  left: BorderSide(width: 1.0, color: Colors.black),
                  right: BorderSide(width: 1.0, color: Colors.black),
                  bottom: BorderSide(width: 1.0, color: Colors.black),
                ),
                color: Color.fromRGBO(255, 255, 255, 0.5),
              ),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: DataTable(
                    sortAscending: true,
                    sortColumnIndex: 0,
                    columns: const <DataColumn>[
                      DataColumn(
                        label: Text(
                          'State',
                          style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Top Candy',
                          style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Pounds',
                          style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                        ),
                      ),
              DataColumn(
                label: Text(
                  '2nd Place',
                  style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                ),
              ),
              DataColumn(
                label: Text(
                  'Pounds',
                  style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                ),
              ),
              DataColumn(
                label: Text(
                  'Third Place',
                  style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                ),
              ),
              DataColumn(
                label: Text(
                  'Pounds',
                  style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
                ),
              ),
            ],
            rows: const <DataRow>[
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('AK', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Twix', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('4908', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Milky Way', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('4072', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Blow Pops', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('3988', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('AL', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Candy Corn', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('115269', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Starburst', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('111280', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Skittles', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('98771', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('AR', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Jolly Ranchers', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('228645', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Butterfinger', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('201845', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('Skittles', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                  DataCell(Text('76353', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),)),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('AZ', style: TextStyle(
                fontWeight: FontWeight.bold,
                  )
                  )),
                  DataCell(Text('Hot Tamales', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('751772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Hershey Kisses', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('732991', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Snickers', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('682562', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('CA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Skittles', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('1509827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('1059223', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('1009388', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('CO', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Hershey Kisses', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('129826', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Twix', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('127840', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Milky Way', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('107636', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('CT', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Milky Way', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('2987', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Almond Joy', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('2278', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('1582', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('DC', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('27546', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Tootsie Pops', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('24725', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Blow Pops', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('19827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('DE', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('21336', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Life Savers', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('16703', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Candy Corn', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('11987', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('FL', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('598285', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('501926', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('Starburst', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('410029', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('GA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Swedish Fish", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('145827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('124872', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('92746', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('HI', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('28091', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hershey's Mini Bars", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('25783', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Butterfinger", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('20974', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('IA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('67829', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('59726', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('48352', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('ID', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('90284', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('68546', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('39876', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('IL', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('175867', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Kit Kat", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('139723', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('121845', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('IN', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('99725', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('95638', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('75208', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('KS', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('229857', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('213784', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('189736', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('KY', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Swedish Fish	", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('75254', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('50272', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('33748', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('LA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Lemonheads", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('115789', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Blow Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('101889', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('82675', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('70772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Butterfinger", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('72838', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Dubble Bubble Gum", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('47282', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MD', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('36887', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hershey Kisses", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('30423', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Milky Way", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('24817', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('ME', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('59873', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('51899', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('33776', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MI', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('115269', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('111280', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('98771', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MN', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('190827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('178092', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('120938', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MO', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Milky Way", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('45221', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Almond Joy", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('36782', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Dubble Bubble Gum", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('36772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MS', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("3 Musketeers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('95727', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('94772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Butterfinger", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('68683', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('MT', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Dubble Bubble Gum", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('29773', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Twix", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('21863', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('16729', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NC', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('85779', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('84322', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('78772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('ND', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('61837', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('56745', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('49883', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NE', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('98486', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Salt Water Taffy", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('89734', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Twix", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('35620', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NH', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('69692', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('59822', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('35775', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NJ', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('141782', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('137992', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('130098', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NM', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('80092', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('74637', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Milky Way", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('60829', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NV', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hershey Kisses", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('310974', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('286464', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('219836', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('NY', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('150325', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('142677', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('65820', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('OH', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Blow Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('170236', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('156773', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('140772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('OK', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Dubble Bubble Gum", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('19232', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('14990', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('12109', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('OR', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('89772', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('80726', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('60827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('PA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hershey's Mini Bars", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('275617', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('249827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('182093', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('RI', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Twix", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('17827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('15627', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('12363', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('SC', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Butterfinger", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('119664', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('113847', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('70838', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('SD', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('26371', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('19217', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('13826', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('TN', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('55627', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Salt Water Taffy", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('48888', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('38991', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('TX', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('984627', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('972849', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Sour Patch Kids", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('659870', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('UT', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Jolly Ranchers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('413099', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('260378', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Candy Corn", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('240993', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('VA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Snickers", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('165255', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('150993', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('99009', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('VT', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('34670', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("M&M's", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('24536', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Milky Way", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('19909', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('WA', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Tootsie Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('205882', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Salt Water Taffy", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('180722', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Skittles", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('90828', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('WI', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Starburst", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('110827', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Butterfinger", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('98272', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hot Tamales", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('63829', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('WV', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Hershey's Mini Bars", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('39812', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Blow Pops", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('38747', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Milky Way", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('32884', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text('WY', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Salt Water Taffy", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('25864', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Reese's Cups", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('24790', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text("Dubble Bubble Gum", style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                  DataCell(Text('18776', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ))),
                ],
              ),
            ],
          ),
        ),
      ),
    ),
    ),
    ),
      ),
      ),
        ]
    );
  }
}